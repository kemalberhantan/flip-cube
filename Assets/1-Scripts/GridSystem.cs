﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class GridSystem : MonoBehaviour
{
    public int gridWidth = 0;
    public int gridHeight = 0;

    [SerializeField] public float size = 1f;
    [SerializeField] public bool EditorMode = false;
    public float Size
    {
        get
        {
            if (size != 0)
                return size;

            else
            {
                size = 1f;
                return size;
            }
        }
    }

    public Vector3 GetNearestPoint(Vector3 Pos)
    {
        Pos -= transform.position;
        int xCount = Mathf.RoundToInt(Pos.x / Size);
        int yCount = Mathf.RoundToInt(Pos.y / Size);
        int zCount = Mathf.RoundToInt(Pos.z / Size);
        Vector3 result = new Vector3((float)xCount * Size, (float)yCount * Size, (float)zCount * Size);
        result += transform.position;
        return result;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        for (float x = -22; x < gridWidth; x += Size)
            for (float z = -22; z < gridHeight; z += Size)
            {
                Vector3 point = GetNearestPoint(new Vector3(x, 0.1f, z));
                if (EditorMode)
                {
                    Vector3 from = new Vector3(x, 0.1f, z);
                    Vector3 to1 = new Vector3(x + Size, 0.1f, z);
                    Vector3 to2 = new Vector3(x, 0.1f, z + Size);
                    Gizmos.DrawLine(from, to1);
                    Gizmos.DrawLine(from, to2);
                }
            }
    }
}
